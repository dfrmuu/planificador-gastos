import {useState,useEffect} from 'react'

function Filtros({filtros,setFiltros}) {
  return (
    <>
        <div className="flex justify-center items-center w-full mt-8">
            <div className="px-4 py-3 h-[8rem] md:py-3  md:px-14 md:w-[35rem] xl:w-[55rem]  md:h-[5rem] xl:h-[10rem] border-4 border-blue-300 bg-white rounded shadow-xl text-base xl:text-xl">
                <form className="grid grid-cols-1 h-full md:grid-cols-2 place-content-center items-center gap-4">
                <div className="place-self-center text-center text-base md:text-lg xl:text-2xl">
                    <label className="text-blue-600">Filtros gastos:</label>
                </div>
                <div className="w-full">
                    <select
                    value={filtros ?? ''}
                    className="rounded p-3 border-2 w-full border-blue-200 shadow mt-2 md:mt-0"
                    onChange={(e) => setFiltros(e.target.value)} >
                        <option value="">--Todas las categorías--</option>
                        <option value="ahorro">Ahorro</option>
                        <option value="comida">Comida</option>
                        <option value="casa">Casa</option>
                        <option value="gastos">Gastos varios</option>
                        <option value="ocio">Ocio</option>
                        <option value="salud">Salud</option>
                        <option value="suscripciones">Suscripciones</option>
                    </select>
                </div>
                </form>
            </div>
        </div>
    </>
  )
}

export default Filtros