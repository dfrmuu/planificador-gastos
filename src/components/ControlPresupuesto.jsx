import { useEffect, useState } from "react";
import Swal from 'sweetalert2'
import { buildStyles, CircularProgressbar, } from "react-circular-progressbar";
import 'react-circular-progressbar/dist/styles.css'

function ControlPresupuesto({presupuesto,gastos,setGastos, setPresupuesto, setIsValidPresupuesto}) {

    const [disponible, setDisponible] = useState(0)
    const [gastado, setGastado] = useState(0)
    const [porcentaje, setPorcentaje] = useState(0)

    useEffect(() => {
        const totalGastado = gastos.reduce( (total, gasto) => gasto.cantidad + total, 0)
      
        const totalDisponible = presupuesto - totalGastado

        //Calculo porcentaje
        const calculoPorcentaje = (((presupuesto - totalDisponible) / presupuesto) * 100).toFixed(2)

        setGastado(totalGastado)
        setDisponible(totalDisponible)

        setTimeout(() => {
          setPorcentaje(calculoPorcentaje)
        }, 1000);

    }, [gastos])
    
    function handleReset(e) {
        Swal.fire({
          title: 'Estas seguro?',
          text: "Este botón reiniciara la aplicación",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3b82f6',
          cancelButtonColor: '#EC4899',
          confirmButtonText: 'Si!',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.isConfirmed) {
              setGastos([])
              setPresupuesto(0)
              setIsValidPresupuesto(false)
          }
        })      
    }

    function formatearCantidad(cantidad) {

      return cantidad.toLocaleString('cop-co',{
          style:'currency',
          currency: 'COP'
      })
    }

    return (
      <div className="px-5 py-3 mb-2 relative md:py-3 md:px-14 mt-8 w-[18rem] md:w-[35rem]  xl:w-[55rem] h-[28rem] md:h-[30rem]  xl:h-[27rem] border-4 border-blue-300 bg-slate-50 rounded shadow-xl text-2xl">
          <div className="grid grid-cols-1 xl:grid-cols-2 h-full place-content-center place-items-center">
            <div className="w-[9rem] md:w-[13rem] xl:w-[20rem] self-center">
                  <CircularProgressbar 
                  styles={buildStyles({
                      textSize: '9px',
                      pathColor: porcentaje > 100 ? '#EC4899 ' : '#3b82f6', 
                      textColor: porcentaje > 100 ? '#EC4899 ' : '#3b82f6',
                  })} 
                  text = {`${porcentaje} % Gastado`}
                  value={porcentaje}
                  >

                  </CircularProgressbar>
              </div>
              <div className="text-base md:text-lg mt-6 xl:text-2xl">
                  <button
                      className="bg-pink-500 hover:bg-pink-600 transition-colors shadow text-white mb-5 rounded-lg p-3 w-full"
                      onClick={handleReset}>
                      Reiniciar aplicación
                  </button>

                  <div className="text-center text-base md:text-lg xl:text-xl xl:text-start md:mt-2 xl:mt-14">
                    <p className="mb-5">
                        <span className="text-blue-600">Presupuesto:</span> {formatearCantidad(Number(presupuesto))}
                    </p>
                    <p className={`${disponible < 0 ? 'text-blue-700 mb-5' : 'text-blue-600 mb-5'} `}>
                        <span className="text-blue-600">Disponible:</span> {formatearCantidad(Number(disponible))}
                    </p>
                    <p>
                        <span className="text-blue-600">Gastado:</span> {formatearCantidad(Number(gastado))}
                    </p>
                  </div>
              </div>
          </div>  
      </div>
    )
}

export default ControlPresupuesto