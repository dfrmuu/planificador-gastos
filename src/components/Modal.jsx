import { useState, useEffect } from "react";
import CerrarBtn from "../img/cerrar.svg";
import Mensaje from "./Mensaje";
import { Description, Dialog, DialogBackdrop, DialogPanel, DialogTitle } from '@headlessui/react'

function Modal({isOpen,setIsOpen,setModal,guardarGasto,gastoEditar,setGastoEditar}) {

  const [nombre, setNombre] = useState('')
  const [cantidad, setCantidad] = useState('')
  const [categoria, setCategoria] = useState('')
  const [mensaje, setMensaje] = useState('')
  const [id, setId] = useState('')
  const [fecha, setFecha] = useState('')

  useEffect(() => {
    if (Object.keys(gastoEditar).length > 0) {
      setNombre(gastoEditar.nombre)
      setCantidad(gastoEditar.cantidad)
      setCategoria(gastoEditar.categoria)
      setId(gastoEditar.id)
      setFecha(gastoEditar.fecha)
    }
  }, [gastoEditar])
  

  function handleSubmit(e) {
      e.preventDefault()

      if ([nombre,cantidad,categoria].includes('')) {
        setMensaje('Todos los campos son obligatorios')

        setTimeout(() => {
          setMensaje('')
        }, 2000);

        return;
      }
      
      const objetoGasto = {
        nombre,
        cantidad,
        categoria,
        id,
        fecha
      }


      guardarGasto(objetoGasto)
  }

  function ocultarModal(){
    setAnimarModal(false)
    setGastoEditar({})

    setTimeout(() => {
      setModal(false)
    }, 500);
  }

  return (
    <Dialog open={isOpen} onClose={() => setIsOpen(false)}>

        <DialogBackdrop className="bg-blue-300 opacity-60 fixed inset-0"/>

        <div className="fixed inset-0 flex w-screen items-center justify-center p-6">
          <DialogPanel className="max-w-lg space-y-4 w-[40rem] bg-white border-3 shadow-lg rounded-lg">
              <DialogTitle className="text-2xl text-center uppercase font-semibold text-white p-5 bg-blue-500 rounded-t-lg">
                {gastoEditar.nombre ? 'Editar gasto' : 'Agregar gasto'}
              </DialogTitle>

              <form onSubmit={handleSubmit} className="p-6">

                {mensaje && <Mensaje tipo='error'>{mensaje}</Mensaje>}

                <div className="grid grid-cols-1 text-lg ">
                    <div className="place-self-center mb-3 text-start w-full">
                      <label className="font-semibold">Nombre gasto: </label>
                    </div>

                    <div>
                      <input id="nombre"
                        className="border-4 rounded p-3 w-full"
                        type="text"
                        placeholder="Agrege aqui el nombre"
                        value={nombre}
                        onChange={e => setNombre(e.target.value)}/>
                    </div>
                </div>


                <div className="grid grid-cols-1 text-lg mt-4">
                    <div className="place-self-center mb-3 text-start w-full">
                      <label className="font-semibold">Cantidad gasto:</label>
                    </div>

                    <div>
                      <input id="cantidad"
                          className="border-4 rounded p-3 w-full"
                          type="number"
                          placeholder="Agrege aqui la cantidad ej. 3000COP"
                          value={cantidad}
                          onChange={e => setCantidad(Number(e.target.value))}/>
                    </div>
                </div>

                <div className="grid grid-cols-1 text-lg mt-4">
                    <div className="place-self-center mb-3 text-start w-full">
                      <label className="font-semibold">Seleccione la categoría: </label>
                    </div>

                    <div>
                      <select id="categoria"
                              className="border-4 rounded p-3 w-full" 
                              value={categoria} 
                              onChange={e => setCategoria(e.target.value)}>

                              <option value="">--Seleccione la categoria--</option>
                              <option value="ahorro">Ahorro</option>
                              <option value="comida">Comida</option>
                              <option value="casa">Casa</option>
                              <option value="gastos">Gastos varios</option>
                              <option value="ocio">Ocio</option>
                              <option value="salud">Salud</option>
                              <option value="suscripciones">Suscripciones</option>
                        </select>
                    </div>
                </div>

                <div className="grid grid-cols-1 text-lg mt-6">
                    <div>
                      <input type="submit"
                            className="rounded p-3 w-full bg-blue-500 hover:bg-blue-600 cursor-pointer text-white font-semibold"
                            value={gastoEditar.nombre ? 'Guardar cambios' : 'Guardar gasto'}/> 
                    </div>
                </div>
              </form>

          </DialogPanel>
        </div>
    </Dialog>
  )
}

export default Modal