import Gasto from "./Gasto"

function ListadoGastos({gastos,setGastoEditar,eliminarGasto,filtros,gastosFiltrados}) {
  return (
    <div>
        { filtros ? (
          <>
              <div className="px-5 py-3 md:py-3 md:px-14 mt-8 border-blue-300 w-[18rem] md:w-[35rem] xl:w-[55rem] h-[5rem] md:h-[5rem] bg-blue-50  xl:h-[5rem] 
                              border-4 rounded shadow-xl text-xl">              
                  <div className="flex justify-center h-full w-full">
                      <p className="self-center text-base md:text-lg xl:text-xl text-blue-600">
                        {gastosFiltrados.length > 0 ? 'Gastos' : 'No hay gastos'} 
                      </p>
                  </div>
              </div>

              <br/>

              {gastosFiltrados.map( gasto => (
                <Gasto
                      key={gasto.id}
                      gasto = {gasto}
                      setGastoEditar = {setGastoEditar}
                      eliminarGasto={eliminarGasto}
                />
              ))}
          </>
        ): (
          <>
              <div className="px-5 py-3 md:py-3 md:px-14 mt-8 border-blue-300 w-[18rem] md:w-[35rem] xl:w-[55rem] h-[5rem] md:h-[5rem] bg-blue-50  xl:h-[5rem] 
                              border-4 rounded shadow-xl text-xl">
                  <div className="flex justify-center h-full w-full">
                      <p className="self-center text-base md:text-lg xl:text-xl text-blue-600">
                        {gastos.length > 0 ? 'Gastos' : 'No hay gastos'} 
                      </p>
                  </div>
              </div>

              <br/>

              {gastos.map( gasto => (
                  <Gasto
                      key={gasto.id}
                      gasto = {gasto}
                      setGastoEditar = {setGastoEditar}
                      eliminarGasto={eliminarGasto}
                  />
              ))}
          </>
        )}
    </div>
  )
}

export default ListadoGastos