import { useState } from "react";
import Mensaje from "./Mensaje"
import { FaCoins, FaMoneyBill } from "react-icons/fa"

function NuevoPresupuesto({presupuesto,setPresupuesto,setIsValidPresupuesto}) {

    const [mensaje,setMensaje] = useState('')

    function handlePresupuesto(e) {
          e.preventDefault()

          if (!presupuesto || presupuesto < 0) {
                  setMensaje('No es un presupuesto valido')
                  return
          }

          setMensaje('')
          setIsValidPresupuesto(true)
    }


    return (
      <div className="px-5 py-2 relative md:py-3 md:px-14 mt-8 w-[20rem] md:w-[35rem] xl:w-[35rem] h-[30rem] md:h-[30rem]  xl:h-[32rem] border-4 border-blue-300 bg-slate-50 rounded shadow-xl text-2xl">
          <form onSubmit={handlePresupuesto} className="grid grid-cols-1 place-content-center h-full">

                  <div className="text-center justify-center flex text-xl md:text-2xl xl:text-3xl text-blue-600 mb-5">
                      <FaCoins/>
                      <span className="ms-3">Definir presupuesto</span>
                  </div>

                  <input
                        className="rounded shadow p-3 border-4 text-xl md:text-2xl xl:text-3xl text-center text-blue-500" 
                        type="number"
                        placeholder="Asigne presupuesto a utilizar"
                        value={presupuesto}
                        onChange={e => setPresupuesto(e.target.value)}
                  />

                  <input className="bg-blue-500 hover:bg-blue-700 cursor-pointer text-lg md:text-xl xl:text-2xl
                                    text-white rounded p-3 mt-7"
                        type="submit"
                        value="Añadir"/>

              {mensaje && <Mensaje tipo={'error'}>{mensaje}</Mensaje>}
          </form>
      </div>
    )
}

export default NuevoPresupuesto