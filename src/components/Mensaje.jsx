
function Mensaje({children,tipo}) {
  return (
    <div className={`bg-pink-500 text-white font-semibold text-center p-4 rounded mb-3 mt-3 shadow ${tipo}`}>
        <div> {children}</div>
    </div>
  )
}

export default Mensaje