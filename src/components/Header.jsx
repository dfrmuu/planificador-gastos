import ControlPresupuesto from "./ControlPresupuesto"
import NuevoPresupuesto from "./NuevoPresupuesto"

function Header({nuevoPresupuesto,presupuesto,setPresupuesto,isValidPresupuesto,setIsValidPresupuesto,gastos,setGastos}) {
  return (
    <>
      <header className="relative bg-gradient-to-b from-blue-600 to-transparent flex flex-col items-center">

          <h1 className="text-2xl md:text-3xl xl:text-5xl mb-3 mt-5 text-white text-center"> 
            Planificador de gastos 
          </h1>
          <span className="underline text-sm text-blue-50"> Por Daniel Roa</span>

          {isValidPresupuesto ? (
              <ControlPresupuesto
              presupuesto={presupuesto}
              gastos={gastos}
              setGastos = {setGastos}
              setPresupuesto = {setPresupuesto}
              setIsValidPresupuesto = {setIsValidPresupuesto}
              />
          ):(
              <NuevoPresupuesto
              presupuesto = {presupuesto}
              setPresupuesto = {setPresupuesto}
              setIsValidPresupuesto = {setIsValidPresupuesto}
              />
          )}

      </header>
    </>
  )
}

export default Header