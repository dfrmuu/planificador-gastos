import { 
    LeadingActions,
    SwipeableList,
    SwipeableListItem,
    SwipeAction,
    TrailingActions
} from "react-swipeable-list";
import "react-swipeable-list/dist/styles.css";
import { formatearFecha } from '../helpers';

import IconoAhorro from '../img/icono_ahorro.svg';
import IconoCasa from '../img/icono_casa.svg';
import IconoComida from '../img/icono_comida.svg';
import IconoGastos from '../img/icono_gastos.svg';
import IconoOcio from '../img/icono_ocio.svg';
import IconoSalud from '../img/icono_salud.svg';
import IconoSuscripciones from '../img/icono_suscripciones.svg';

function Gasto({gasto,setGastoEditar,eliminarGasto,}) {
  const {categoria,nombre,cantidad,fecha,id} = gasto

  const diccionarioIconos =  {
    ahorro : IconoAhorro,
    comida : IconoCasa,
    casa : IconoComida,
    gastos : IconoGastos,
    ocio : IconoOcio ,
    salud : IconoSalud,
    suscripciones : IconoSuscripciones
  }

  const leadingActions = () => (
        <LeadingActions>
            <SwipeAction 
            onClick={() => setGastoEditar(gasto)}>

                    Editar
            </SwipeAction>
        </LeadingActions>
  )

  const trailingActions = () => (
        <TrailingActions>
            <SwipeAction 
            onClick={() => eliminarGasto(id)}
            destructive={true}>

                    Borrar
            </SwipeAction>
        </TrailingActions>
  )

  return (
    <>
        <SwipeableList>
            <SwipeableListItem className="mt-1"
                leadingActions={leadingActions()}
                trailingActions={trailingActions()}>
                <div className="cursor-pointer w-[18rem] md:w-[35rem] xl:w-[55rem] h-[19rem] md:h-[15rem]  xl:h-[10rem] border-4 border-blue-200 bg-white rounded shadow-xl text-xl">
                    <div className="grid grid-cols-1 md:grid-cols-3 justify-items-center items-center h-full ">
                        <div className="">
                            <img src={diccionarioIconos[categoria]} alt='Imagén categoria'
                                className="w-[5rem] h-[5rem]  md:w-[10rem] md:h-[7rem]"/>
                        </div>
                        <div className="text-sm md:text-lg xl:text-xl text-center md:text-start w-full text-slate-500">
                            <p className="uppercase font-semibold">
                                {categoria}
                            </p>
                            <p>
                                {nombre}
                            </p>
                            <p>
                                <p className="font-semibold text-lg">Agregado el: {''}</p>
                                {formatearFecha(fecha)}
                            </p>
                        </div> 
                        <div className="self-center">
                            <p className='text-center font-semibold text-2xl'>COP {cantidad}</p>
                        </div>
                    </div>

                </div>
            </SwipeableListItem>
        </SwipeableList>

        <br/>
    </>
  )
}

export default Gasto