import { useState, useEffect} from 'react'
import { generarId} from "./helpers";
import Header from './components/Header'
import ListadoGastos from './components/ListadoGastos'
import Modal from './components/Modal'
import IconoNuevo from './img/nuevo-gasto.svg'
import Filtros from './components/Filtros';
import { FaPlus } from 'react-icons/fa';

function App() {

    const [nuevoPresupuesto,setNuevoPresupuesto] = useState(true)

    const [presupuesto,setPresupuesto] = useState(
      Number(localStorage.getItem('presupuesto')) ?? 0
    )
    const [isValidPresupuesto, setIsValidPresupuesto] = useState(false)
    const [modal, setModal] = useState(false)
    const [animarModal, setAnimarModal] = useState(false)
    const [filtros,setFiltros] = useState('')
    const [gastosFiltrados,setGastosFiltrados] = useState([])

    const [gastos,setGastos] = useState([
        ...(JSON.parse(localStorage.getItem('gastos')) ?? [])
    ])
    const [gastoEditar, setGastoEditar] = useState({})

    useEffect(() => {
      if (Object.keys(gastoEditar).length > 0) {
        setModal(true)

        setTimeout(() => {
          setAnimarModal(true)
        }, 500);
      }
    }, [gastoEditar])

    useEffect(() => {
      if (filtros) {
        const gastosFiltrados = gastos.filter( gasto => gasto.categoria === filtros )
        setGastosFiltrados(gastosFiltrados)
      }
    }, [filtros])
    

    useEffect(() => {
      localStorage.setItem('presupuesto', presupuesto ??  0)
    }, [presupuesto])

    
    useEffect(() => {
      localStorage.setItem('gastos', JSON.stringify(gastos) ??  [])
    }, [gastos])
    
    
    useEffect(() => {
      const presupuestoLS = Number(localStorage.getItem('presupuesto')) ?? 0

      if (presupuestoLS > 0) {
        setIsValidPresupuesto(true)
      }
    }, [])

    
    function handleModal() {
        setModal(true)
        setGastoEditar({})
    }

    function guardarGasto (gasto) {

        if (gasto.id) {
          //Actualizar gasto
          const gastosActualizados = gastos.map( gastoState => gastoState.id === gasto.id ? gasto : gastoState)
          setGastos(gastosActualizados)
          setGastoEditar({})

        }else{
          //Nuevo gasto
          gasto.id = generarId()
          gasto.fecha = Date.now()
    
          setGastos([...gastos, gasto])
        }

        setAnimarModal(false)

        setTimeout(() => {
          setModal(false)
        }, 500);
    }

    function eliminarGasto(id){
        const gastosActualizados = gastos.filter( gastoState => gastoState.id !== id);

        setGastos(gastosActualizados)
    }

    return (
      <>
          
        {isValidPresupuesto && (
          <div className='fixed bottom-0 right-0 p-5 z-20'>
            <button className='bg-blue-500 hover:bg-blue-600 transition-colors p-5 text-white rounded-full border-3 shadow' onClick={() => handleModal()}>
                <FaPlus className='text-2xl'/>
            </button>
          </div>
        )}

        <div className='relative h-dvd w-full font-[Poppins]'>
            <div className='mb-10'>
              <Header presupuesto ={presupuesto}
                setPresupuesto = {setPresupuesto}
                isValidPresupuesto = {isValidPresupuesto}
                setIsValidPresupuesto = {setIsValidPresupuesto}
                gastos = {gastos}
                setGastos = {setGastos}/>

                {isValidPresupuesto && (
                  <div className='flex flex-col flex-grow w-full items-center justify-start'>
                    <Filtros filtros = {filtros} setFiltros = {setFiltros}/>  
                    <main>
                      <ListadoGastos
                        gastos = {gastos}
                        setGastoEditar = {setGastoEditar}
                        eliminarGasto = {eliminarGasto}
                        filtros = {filtros}
                        gastosFiltrados = {gastosFiltrados}
                        setGastosFiltrados = {gastosFiltrados}
                      />
                    </main>
                  </div>
                )}

                {modal && <Modal isOpen={modal}
                            setIsOpen={setModal} 
                            guardarGasto = {guardarGasto}
                            gastoEditar = {gastoEditar}
                            setGastoEditar = {setGastoEditar}/> }
          </div>
        </div>
    </>
    )
}

export default App
